# start from the rocker/r-ver:4.2.1 image
FROM rocker/r-ver:4.2.1

# install the linux libraries needed for plumber
RUN apt-get update -qq && apt-get install -y \
  libssl-dev \
  libcurl4-gnutls-dev \
  unixodbc \
  unixodbc-dev \
  r-cran-rodbc \
  curl

# Driver SQL Server
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/11/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql17
RUN ACCEPT_EULA=Y apt-get install -y mssql-tools
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc

# install plumber y SQL Server Driver en R
RUN R -e "install.packages('plumber')"
RUN R -e "install.packages('RODBC')"
RUN R -e "install.packages('odbc')"
RUN R -e "install.packages('dplyr')"
RUN R -e "install.packages('tidyverse')"
RUN R -e "install.packages('writexl')"
RUN R -e "install.packages('readxl')"
RUN R -e "install.packages('lubridate')"
RUN R -e "install.packages('glue')"
RUN R -e "install.packages('DBI')"
RUN R -e "install.packages('readr')"
RUN R -e "install.packages('data.table')"
RUN R -e "install.packages('caret')"
RUN R -e "install.packages('quantreg')"
RUN R -e "install.packages('jtools')"
RUN R -e "install.packages('GGally')"
RUN R -e "install.packages('pROC')"
RUN R -e "install.packages('ROSE')"
RUN R -e "install.packages('randomForest')"
RUN R -e "install.packages('party')"


# copy everything from the current directory into the container
COPY / /

# open port 3000 to traffic
EXPOSE 3000
ENV PORT 3000
ENV HOST 0.0.0.0

# when the container starts, start the main.R script
ENTRYPOINT ["Rscript", "main.R"]

CMD [ "runserver", "0.0.0.0:3000" ]