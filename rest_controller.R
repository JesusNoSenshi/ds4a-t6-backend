#* @apiTitle DS4A Team 6
#' @apiDescription DS4A Team 6 BackEnd
#' @apiVersion 1.0

#* @filter cors
cors <- function(res) {
  res$setHeader("Access-Control-Allow-Origin", "*")
  plumber::forward()
}

#* Top 3 Ciudades con mayores casos
#* @get /Top3Ciudades
top3Cities <- function(){
  load(file="Data/topcity.RData")
  topcity
}

#* Logistic Regression
#* @get /PredictLR
predict_logistic <- function(Genero, Edad, IMC, Glucosa, Colesterol){
  logistic <- readRDS("Models/logistic.rds")
  input <- data.frame(Genero = toupper(Genero), Edad = as.double(Edad), avgIMC = as.double(IMC), stdIMC = 1, avgGlucosa = as.double(Glucosa), avgColesterolTotal= as.double(Colesterol))
  #predict(logistic,input)
  logistic %>% predict(input,type = "response")
}


#* Variables con mayor influencia Logistic
#* @get /LRTopEst
topIncidentes <- function(){
  library(dplyr)
  logistic <- readRDS("Models/logistic.rds")
  exp(logistic$coefficients) %>% as.data.frame() %>% arrange(.,desc(.)) %>% head(3)
}

#* Random Forest
#* @get /PredictRF
predict_randomForest <- function(Genero, Edad, IMC, Glucosa, Colesterol){
  library(randomForest)
  load(file="Models/randomF.RData")
  input <- data.frame(Genero = toupper(Genero), Edad = as.double(Edad), avgIMC = as.double(IMC), stdIMC = 1, avgGlucosa = as.double(Glucosa), avgColesterolTotal= as.double(Colesterol))
  predict(randomF2,input,type = 'prob')
}


#* Random Forest Metrics Accuracy
#* @get /RFMetrics_accuracy
metrics_randomForest <- function(){
  load(file="Models/confusion.RData")
  accuracy <- cm$overall[1]
}

#* Random Forest Metrics Sensitivity
#* @get /RFMetrics_sensitivity
metrics_randomForest <- function(){
  load(file="Models/confusion.RData")
  sensitividad <- cm$byClass[1]
}

#* Random Forest Metrics Specificity
#* @get /RFMetrics_specificity
metrics_randomForest <- function(){
  load(file="Models/confusion.RData")
  especifidad <- cm$byClass[2]
}

#* Random Forest Variable Importance
#* @get /RF_VarImp
VarImp_randomForest <- function(){
  load(file="Models/randomF.RData")
  library(caret)
  library(dplyr)
  varImp <- varImp(randomF2)
  varImp$importance %>% arrange(.,desc(.))
}